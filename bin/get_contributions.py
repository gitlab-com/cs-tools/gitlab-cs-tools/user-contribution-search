import json
import argparse
import gitlab
import os.path
import markdown
from datetime import datetime, timedelta
import re

def create_pipeline_schedule(project):
    if not project.pipelineschedules.list():
        print("No pipeline schedule found, creating...")
        cron = "0 0 */7 * *"
        sched = project.pipelineschedules.create({
            'ref': "main",
            'description': 'Update contribution search',
            'cron': cron})
        sched.variables.create({'key':'POST_UPDATES','type':'env_var','value':'True'})
    else:
        print("Pipeline schedule found.")

def set_pages_access_level(project):
    print("Validating pages access level...")
    if project.pages_access_level != "private":
        print("Setting pages access level to private")
        project.pages_access_level = "private"
        project.save()
    else:
        print("Pages access level is private.")

def clean_description(desc_string):
    desc_string = re.sub("<img.*>", "", desc_string)
    # desc_string = desc_string.replace("\n"," ")
    md = markdown.Markdown()
    desc_string = md.convert(desc_string)
    return desc_string

def generate_link(event):
    link = ""
    if event["type"] == "Issue":
        link = event["project_url"] + "/-/issues/" + str(event["iid"])
    if event["type"] == "MergeRequest":
        link = event["project_url"] + "/-/merge_requests/" + str(event["iid"])
    if event["type"] == "DiscussionNote":
        link = event["project_url"] + "/-/issues/" + str(event["noteable_iid"]) + "#note_" + str(event["iid"])
    if event["type"] == "Note" and event["noteable_type"] == "MergeRequest":
        link = event["project_url"] + "/-/merge_requests/" + str(event["noteable_iid"]) + "#note_" + str(event["iid"])
    if event["type"] == "Note" and event["noteable_type"] == "Issue":
        link = event["project_url"] + "/-/issues/" + str(event["noteable_iid"]) + "#note_" + str(event["iid"])
    if event["type"] == "DiffNote":
        link = event["project_url"] + "/-/merge_requests/" + str(event["noteable_iid"]) + "#note_" + str(event["iid"])
    if event["type"] == "Commit":
        link = event["project_url"] + "/-/commit/" + str(event["iid"])
    return link

parser = argparse.ArgumentParser(description='Get all contributions of a user incrementally')
parser.add_argument('token', help='GitLab personal access token with api and write_repository permissions')
parser.add_argument('user', help='User ID of the user to get events for')
parser.add_argument('--project', help='The ID of this project')

args = parser.parse_args()

gl = gitlab.Gitlab("https://gitlab.com", private_token=args.token)

user = gl.users.get(args.user)

events_objects = []
event_ids = set()
projects = {}

# configuring the project automatically to avoid leaks
if args.project:
    project = gl.projects.get(args.project)
    create_pipeline_schedule(project)
    set_pages_access_level(project)
    pipelines = project.pipelines.list()
    # new fork, delete the events file
    if len(pipelines) <= 1:
        os.remove("events.json")

last_contribution_date = None
if os.path.isfile("events.json"):
    with open("events.json", "r") as eventsfile:
        events_objects = json.load(eventsfile)
        if events_objects:
            # lets start the day before the last event, to make sure we are not missing anything
            last_contribution_date = datetime.strptime(events_objects[0]["date"], "%Y-%m-%d") - timedelta(days=1)
            last_contribution_date = last_contribution_date.isoformat()
            for event in events_objects:
                event_ids.add(event["id"])

events = None
if last_contribution_date:
    events = user.events.list(as_list=False, after=last_contribution_date)
else:
    events = user.events.list(as_list=False)

for event in events:
    if event.id not in event_ids:
        if event.attributes["action_name"] in ["created","deleted","pushed new", "joined","left","imported","closed"]:
            continue
        if event.attributes["target_type"] in ["WikiPage::Meta","Milestone","DesignManagement::Design"]:
            continue
        event_object = {"title":event.attributes["target_title"],
            "date":event.attributes["created_at"][0:event.attributes["created_at"].find("T")],
            "type":event.attributes["target_type"],
            "action":event.attributes["action_name"],
            "iid":event.attributes["target_iid"],
            "id":event.attributes["id"]}
        if event.attributes["target_type"] == "DiscussionNote":
            event_object["type"] == "Note"
        if "push_data" in event.attributes:
            event_object["title"] = event.attributes["push_data"]["commit_title"]
            event_object["type"] = "Commit"
            event_object["iid"] = event.attributes["push_data"]["commit_to"]
        if "note" in event.attributes:
            event_object["description"] = clean_description(event.attributes["note"]["body"])
            event_object["noteable_iid"] = event.attributes["note"]["noteable_iid"]
            event_object["noteable_type"] = event.attributes["note"]["noteable_type"]
        else:
            event_object["description"] = ""
        if event.attributes["project_id"] not in projects:
            project = gl.projects.get(event.attributes["project_id"])
            projects[event.attributes["project_id"]] = project.web_url
        event_object["project_url"] = projects[event.attributes["project_id"]]
        event_object["web_url"] = generate_link(event_object)
        events_objects.append(event_object)

sorted_events = sorted(events_objects, key=lambda d: d['date'], reverse=True)
# for event_object in events_objects:
#     if "note" in event_object:
#         event_object["note"]["body"] = clean_description(event_object["note"]["body"])

with open("events.json", "w") as eventsfile:
    json.dump(sorted_events, eventsfile, indent=2)

with open("template/index.html","rt") as templatefile:
    with open("public/index.html","wt") as outputfile:
        for line in templatefile:
            outputfile.write(line.replace('%USERNAME%', user.username))
