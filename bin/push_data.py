import gitlab
import os
import argparse

def push_data(gl, project, branch, action):
    try:
        data = {
            'branch': branch,
            'commit_message': "[ci skip] push data file",
            'actions': [
                {
                    'action': action,
                    'file_path': 'events.json',
                    'content': open("public/events.json").read(),
                }
            ]
        }
        commit = project.commits.create(data)
    except Exception as e:
        raise e

parser = argparse.ArgumentParser(description='Push data to project')
parser.add_argument('token', help='API token able to push to the project repo')
parser.add_argument('gitlab', help='GitLab instance url')
parser.add_argument('project', help='ID of the project to push to')
parser.add_argument('branch', help='The branch to push to')

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)
project = gl.projects.get(args.project)

try:
    print("Pushing new file")
    push_data(gl, project, args.branch, "create")
except gitlab.exceptions.GitlabCreateError as e:
    print("File already exists, updating")
    push_data(gl, project, args.branch, "update")
except Exception as e:
    print("Could not push file to repo")
    print(e)
    exit(1)