# User contribution search

Dive into your past activities and find your previous contributions with this simple pages site.

![screenshot](user_contribution_search.png)

# Usage

* Fork this project. **Set visibility to private.**
* Add a CI variable `GIT_TOKEN`, containing an `api` and `write_repository` scope private access token
  * Navigate to your profile-> `Edit Profile`
  * Select `Access Tokens`
  * Create a new token with `api` and `write_repository` scopes
  * After clicking `Create personal access token`, copy the token string that is displayed on the page
  * Open the forked `User contribution search` project
  * Open `Settings->CI/CD->Variables`
  * Add a variable with key `GIT_TOKEN` and paste your personal access token as the value
  * `Protect` and `Mask` the variable by clicking the respective checkboxes
  * Click `Add variable`
* **Optionally** add a CI variable `USER` containing the numeric user ID of the user you want contribution search for. Otherwise, the user owning the access token will be used. Be aware that the search will only display results that are visibile to the token that is used.
* Run the pipeline
* Browse to Deploy -> Pages to get the pages URL of your new search interface

# Resetting data

The script stores data in the repository as `events.json`. On each run, it finds the last event in the file and crawls user events for any later event. To reset the data, simply delete `events.json` in the repository. The script will then start listing all events on the next run, rebuilding the data file.